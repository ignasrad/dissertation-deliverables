package com.oaj01.app.healthcareapp.fragments;

import androidx.lifecycle.ViewModel;

public class ResetPasswordViewModel extends ViewModel {

    private String code;

    public ResetPasswordViewModel(){
        code = "";
    }

    public String getCode() {
        return code;
    }

    public void setCode(String token){
        code = token;
    }

}
