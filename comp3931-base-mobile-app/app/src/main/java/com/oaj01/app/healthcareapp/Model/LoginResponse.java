package com.oaj01.app.healthcareapp.Model;

import android.content.Context;

import com.oaj01.app.healthcareapp.Storage.SharedPrefManager;

public class LoginResponse {

    private String access_token, refresh_token, error;

    private User user;
    public static Context contextOfApplication;

    public LoginResponse(String access_token, String refresh_token, User user, String error) {
        this.access_token = access_token;
        this.refresh_token = refresh_token;
        this.user = user;
        this.error = error;
    }

    public String getToken() {
        return access_token;
    }

    public void setToken(String token) {
        this.access_token = token;
        SharedPrefManager.getInstance(contextOfApplication).saveToken(this.access_token);
    }

    public String getError() {
        return error;
    }

    public User getUser() {
        return user;
    }
}


