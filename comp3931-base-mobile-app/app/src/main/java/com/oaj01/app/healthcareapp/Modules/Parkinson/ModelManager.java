package com.oaj01.app.healthcareapp.Modules.Parkinson;

import android.content.Context;
import android.widget.Toast;

import com.minh.app.healthcareapp.ml.PDmodel;

import org.tensorflow.lite.DataType;
import org.tensorflow.lite.support.tensorbuffer.TensorBuffer;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


public class ModelManager {

    public float predict(Context context, double[] input){
        // Creates inputs for reference.

        ByteBuffer inputBuffer = ByteBuffer.allocate(Float.BYTES * input.length);
        inputBuffer.order(ByteOrder.LITTLE_ENDIAN);

        for (int i = 0 ; i < input.length; i++)
        {
            inputBuffer.putFloat((float)input[i]);
        }


        try {
            PDmodel model = PDmodel.newInstance(context);

            // Creates inputs for reference.
            TensorBuffer inputFeature0 = TensorBuffer.createFixedSize(new int[]{1, 26}, DataType.FLOAT32);
            inputFeature0.loadBuffer(inputBuffer);

            // Runs model inference and gets result.
            PDmodel.Outputs outputs = model.process(inputFeature0);
            TensorBuffer outputFeature0 = outputs.getOutputFeature0AsTensorBuffer();

            float[] arr = outputFeature0.getFloatArray();

            // Releases model resources if no longer used.
            model.close();
            return arr[0];
        } catch (IOException e) {
            e.printStackTrace();
        }

        return 0;
    }
}

