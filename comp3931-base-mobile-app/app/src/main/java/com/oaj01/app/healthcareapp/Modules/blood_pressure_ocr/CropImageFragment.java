package com.oaj01.app.healthcareapp.Modules.blood_pressure_ocr;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import com.minh.app.healthcareapp.R;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import static android.app.Activity.RESULT_OK;
import static com.oaj01.app.healthcareapp.Modules.common.Utils.getRotateDegree;
/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CropImageFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CropImageFragment extends Fragment {

    private String input_picture;
    private ImageView image_view;
    private View view;
    private Button button_done;
    private Button button_crop;

    public CropImageFragment() { }

    public static CropImageFragment newInstance(String image_uri) {
        CropImageFragment fragment = new CropImageFragment();
        Bundle args = new Bundle();
        args.putString("image", image_uri);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            input_picture = getArguments().getString("image");
            Log.d("debug", "Input image " + Uri.parse(input_picture));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d("debug", "crop image fragment");
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_crop_image, container, false);
        image_view = view.findViewById(R.id.picture_upload);
        button_done = view.findViewById(R.id.button_done);
        button_crop = view.findViewById(R.id.button_crop);
        Picasso.get().load(Uri.parse(input_picture))
                .rotate(getRotateDegree(getContext().getContentResolver(), Uri.parse(input_picture)))
                .fit().centerInside().into(image_view);
        button_crop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { startCropActivity(); }
        });
        button_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), "Not yet implemented", Toast.LENGTH_SHORT).show();
            }
        });
        return view;
    }

    // cannot get input picture in click listener, so create a separate function
    private void startCropActivity() {
        CropImage.activity(Uri.parse(input_picture)).start(getContext(), this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            assert result != null;
            if (result.getUri() != null) {
                Log.d("debug", "New cropped image " + result.getUri());
                image_view.setImageURI(result.getUri());
            }
        }
    }
}