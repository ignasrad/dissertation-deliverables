package com.oaj01.app.healthcareapp.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProviders;

import com.minh.app.healthcareapp.R;
import com.oaj01.app.healthcareapp.API.RetrofitClient;
import com.oaj01.app.healthcareapp.MainActivity;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewPasswordFragment extends Fragment {

    MainActivity main;
    ResetPasswordViewModel viewModel;
    View view;
    TextView password;
    TextView repeatedPassword;
    Button back;
    Button upload;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        main = (MainActivity)getActivity();
        view = inflater.inflate(R.layout.new_password_fragment, container, false);
        back = view.findViewById(R.id.back);
        password = view.findViewById(R.id.password1);
        repeatedPassword = view.findViewById(R.id.password2);
        upload = view.findViewById(R.id.uploadCode);

        viewModel = ViewModelProviders.of(getActivity()).get(ResetPasswordViewModel.class);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //button listeners
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                main.getSupportFragmentManager().popBackStack();
            }
        });

        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = viewModel.getCode();
                String inputPassword = password.getText().toString();
                String inputRepeatedPassword = repeatedPassword.getText().toString();
                if (inputPassword.isEmpty()) {
                    password.setError("Password is required");
                    password.requestFocus();
                    return;
                }
                else if(code.isEmpty()){
                    password.setError("Enter the code");
                    password.requestFocus();
                    return;
                }
                else if (inputPassword.length() < 6) {
                    password.setError("Password must contain at least 6 characters");
                    password.requestFocus();
                    return;
                }else if (!checkPassword(inputPassword)) {
                    password.setError("Password must contain at least one lowercase and uppercase letter and a digit");
                    password.requestFocus();
                    return;
                }
                else if (! inputPassword.equals(inputRepeatedPassword)){
                    repeatedPassword.setError("Passwords do not match");
                    repeatedPassword.requestFocus();
                }

                resetPassword(code, inputPassword);
            }
        });
    }

    private void resetPassword(String token, String newPassword) {
        Call<ResponseBody> call = RetrofitClient
                .getInstance()
                .getApi()
                .changePassword(token, newPassword);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getActivity(), "Your Password Has Been Updated", Toast.LENGTH_LONG).show();
                    main.loadFragment(new LoginFragment());

                    // Clear fragments stack
                    main.getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                } else {
                    Toast.makeText(getActivity(), "Please Enter a Valid Password and Code", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getActivity(), "Failed to update password", Toast.LENGTH_LONG).show();
            }
        });
    }

    private static boolean checkPassword(String password) {
        char currentCharacter;
        boolean numberPresent = false;
        boolean upperCasePresent = false;
        boolean lowerCasePresent = false;

        for (int i = 0; i < password.length(); i++) {
            currentCharacter = password.charAt(i);
            if (Character.isDigit(currentCharacter)) {
                numberPresent = true;
            } else if (Character.isUpperCase(currentCharacter)) {
                upperCasePresent = true;
            } else if (Character.isLowerCase(currentCharacter)) {
                lowerCasePresent = true;
            }
        }
        return numberPresent && upperCasePresent && lowerCasePresent;
    }
}
