package com.oaj01.app.healthcareapp.Modules.Parkinson;

import com.oaj01.app.healthcareapp.Modules.Parkinson.jlibrosa.JLibrosa;
import com.oaj01.app.healthcareapp.Modules.Parkinson.jlibrosa.WavFileException;
import com.oaj01.app.healthcareapp.Modules.Parkinson.jlibrosa.FileFormatNotSupportedException;

import java.io.IOException;

public class FeatureExtraction {

        public double[] retrieveMfccs(String file) {
        int sampleRate = 44100;        // Samples per second
        int duration = 6;        // Seconds

        JLibrosa librosa = new JLibrosa();
        try {
            double[] stds = new double[26];
            float[] signal = librosa.loadAndRead(file, sampleRate, duration);
            System.out.println(signal.length);

            float[][] mfccValues = librosa.generateMFCCFeatures(signal, sampleRate, 13);

            int i = 0;
            for(float[] mfcc : mfccValues){
                double std = getStd(mfcc);
                stds[i] = std;
                i ++;
            }

            for(float[] mfcc : mfccValues){
                double mean = getMean(mfcc);
                stds[i] = mean;
                i ++;
            }

            return stds;



        } catch (IOException e) {
            e.printStackTrace();
        } catch (FileFormatNotSupportedException e) {
            e.printStackTrace();
        } catch (WavFileException e) {
            e.printStackTrace();
        }
        return new double[26];
    }


    private float getMean(float[] input){
        float n=input.length,sum=0,mean;
        for(int i=0;i<n;i++)
        {
            sum=sum+input[i];
        }
        mean=sum/n;

        return mean;
    }

    private float getStd(float[] input){
        float n=input.length,sum=0,mean;
        for(int i=0;i<n;i++)
        {
            sum=sum+input[i];
        }
        mean=sum/n;

        sum=0;
        for(int i=0;i<n;i++)
        {
            sum+=Math.pow((input[i]-mean),2);

        }
        mean=sum/(n-1);
        return (float)Math.sqrt(mean);
    }
}
