package com.oaj01.app.healthcareapp.API;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.oaj01.app.healthcareapp.MainActivity;
import com.oaj01.app.healthcareapp.Storage.SharedPrefManager;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class TokenInterceptor implements Interceptor {

    String bearer;
    public static Context contextOfApplication;

    public TokenInterceptor(String bearer) {
        this.bearer = bearer;
        Log.d("Bearer", bearer);
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        synchronized (this) {
            Request request = chain.request();
            Request authenticatedRequest = request.newBuilder()
                    .header("Authorization", bearer).build();
            Response response = chain.proceed(authenticatedRequest);
            if (response.code() == 401 || response.code() == 403) {
                // refresh token code here
                retrofit2.Response<ResponseBody> newResponse =
                        RetrofitClient.getInstance().getApi().refreshToken().execute();

                if (newResponse.message() == null || newResponse.code() != 200) {
                    Intent intent = new Intent(contextOfApplication, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    contextOfApplication.startActivity(intent);
                    return null;
                }
                else {
                    Log.d("debug", newResponse.message());
                    SharedPrefManager.getInstance(contextOfApplication).saveToken(newResponse.message());
                    Request newAuthRequest = request.newBuilder()
                            .header("Authorization", bearer).build();
                    return chain.proceed(newAuthRequest);
                }
            }
            else return response;
        }
    }
}

