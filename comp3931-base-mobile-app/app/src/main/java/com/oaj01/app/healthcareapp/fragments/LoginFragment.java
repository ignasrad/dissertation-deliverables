package com.oaj01.app.healthcareapp.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.minh.app.healthcareapp.R;
import com.oaj01.app.healthcareapp.API.RetrofitClient;
import com.oaj01.app.healthcareapp.BaseActivity;
import com.oaj01.app.healthcareapp.MainActivity;
import com.oaj01.app.healthcareapp.Model.LoginResponse;
import com.oaj01.app.healthcareapp.Storage.SharedPrefManager;

import androidx.fragment.app.Fragment;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginFragment extends Fragment {
    MainActivity main;
    View view;
    EditText emailEditText;
    EditText passwordEditText;
    Button loginButton;
    TextView forgotPassword;
    Button registerButton;
    String input_email;
    String input_password;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.login_fragment, container, false);
        emailEditText = view.findViewById(R.id.editTextEmail);
        passwordEditText = view.findViewById(R.id.editTextPassword);
        registerButton = view.findViewById(R.id.registerButton);
        loginButton = view.findViewById(R.id.loginButton);
        forgotPassword = view.findViewById(R.id.forgottenPassword);
        main = (MainActivity)getActivity();

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Button listeners
        loginButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                input_email = emailEditText.getText().toString();
                input_password = passwordEditText.getText().toString();
//                login(input_email, input_password);
                Intent intent = new Intent(getActivity(), BaseActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                main.pushNewFragment(new RegisterFragment());
            }
        });

        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                main.pushNewFragment(new PasswordResetFragment());
            }
        });
    }

    private void login(String email, String password) {
        Call<LoginResponse> call = RetrofitClient
                .getInstanceAuth(email, password)
                .getApi()
                .userLogin(email, password);

        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                LoginResponse loginResponse = response.body();

                if(response.code() == 200) {
                    if (loginResponse != null) {
                        String token = loginResponse.getToken();
                        //TODO: Uncomment bottom line when server returns user details on request
                        // We need to save the user in order to have it's ID and Name on the application. However,
                        // /authenticate endpoint of the server, does not return user information at the moment
//                        SharedPrefManager.getInstance(getActivity()).saveUser(loginResponse.getUser());
                        SharedPrefManager.getInstance(getActivity()).saveToken(token);
                    }
                    Intent intent = new Intent(getActivity(), BaseActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
                else if(response.code() == 401) {
                    Toast.makeText(getActivity(), "Incorrect Email or Password", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "Error Logging in", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Toast.makeText(getActivity(), "Call Failure", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
