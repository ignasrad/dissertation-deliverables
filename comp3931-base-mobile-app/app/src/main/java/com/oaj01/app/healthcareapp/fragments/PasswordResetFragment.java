package com.oaj01.app.healthcareapp.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.minh.app.healthcareapp.R;
import com.oaj01.app.healthcareapp.API.RetrofitClient;
import com.oaj01.app.healthcareapp.MainActivity;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PasswordResetFragment extends Fragment {

    MainActivity main;
    View view;
    TextView email;
    Button back;
    Button sendCode;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        main = (MainActivity)getActivity();
        view = inflater.inflate(R.layout.reset_password_fragment, container, false);
        email = view.findViewById(R.id.textField);
        back = view.findViewById(R.id.back);
        sendCode = view.findViewById(R.id.uploadCode);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //button listeners
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                main.getSupportFragmentManager().popBackStack();
            }
        });

        sendCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String inputEmail = email.getText().toString();

                if (inputEmail.isEmpty()) {
                    email.setError("Email is required");
                    email.requestFocus();
                    return;
                }
                resetPassword(inputEmail);
                main.pushNewFragment(new EnterCodeFragment());
            }
        });
    }

    private void resetPassword(String email) {
        Call<ResponseBody> call = RetrofitClient
                .getInstance()
                .getApi()
                .passwordReset(email);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getActivity(), "Email Sent", Toast.LENGTH_LONG).show();

//                    main.pushNewFragment(new EnterCodeFragment());
                } else {
                    Toast.makeText(getActivity(), "Please Enter a Valid Email", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getActivity(), "Failed to Make the Request", Toast.LENGTH_LONG).show();
            }
        });
    }

}
