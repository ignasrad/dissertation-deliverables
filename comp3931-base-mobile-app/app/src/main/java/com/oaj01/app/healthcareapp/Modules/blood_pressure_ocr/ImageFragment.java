package com.oaj01.app.healthcareapp.Modules.blood_pressure_ocr;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.ImageDecoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.minh.app.healthcareapp.R;
import com.oaj01.app.healthcareapp.BaseActivity;

import static com.oaj01.app.healthcareapp.Modules.common.Utils.toUri;

public class ImageFragment extends Fragment {
    private View view;
    private Button take_pic;
    private Button upload_pic;
    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final int REQUEST_IMAGE_GALLERY = 2;
    private BaseActivity baseActivity;
    private Uri image_uri;
    private Bitmap image_bmp;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_image, container, false);
        take_pic = view.findViewById(R.id.take_pic);
        upload_pic = view.findViewById(R.id.upload_pic);
        baseActivity = (BaseActivity) getActivity();
        take_pic.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) { takePicture(); }
        });
        upload_pic.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) { selectPicture(); }
        });
        return view;
    }

    // go to device gallery, wait for user to select an image
    private void selectPicture() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE} , 1);
        } else {
            Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setType("image/*");
            startActivityForResult(intent, REQUEST_IMAGE_GALLERY);
        }
    }

    // activate device camera, if available
    private void takePicture() {
        // need to ask permission from user if it isn't allowed initially
        if (ContextCompat.checkSelfPermission(getContext(),Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        } else {
            Intent cameraIntent = new Intent (MediaStore.ACTION_IMAGE_CAPTURE);
            try {
                startActivityForResult(cameraIntent, REQUEST_IMAGE_CAPTURE);
            } catch (ActivityNotFoundException e) {
                Log.d("debug", e.getStackTrace().toString());
            }
        }
    }

    // get image after selecting or taking a picture
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        Log.d("debug", "Getting image...");
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK &&
                (requestCode == REQUEST_IMAGE_GALLERY || requestCode == REQUEST_IMAGE_CAPTURE)) {
            Log.d("debug", "Got image...");
            assert data != null;
            // in certain version, method of getting image is different from others
            // this is for Android < 8.0
            if (data.getData() != null) {
                image_uri = data.getData();
                Log.d("debug","Uri image " + image_uri.toString());
                take_pic.setEnabled(false);
                upload_pic.setEnabled(false);
                baseActivity.loadNewFragment(CropImageFragment.newInstance(image_uri.toString()));
            } else {
                // Android 8.0 need to get image this way
                image_bmp = (Bitmap) data.getExtras().get("data");
                Log.d("debug","Bitmap image " + image_bmp.toString());
                take_pic.setEnabled(false);
                upload_pic.setEnabled(false);
                baseActivity.loadNewFragment(CropImageFragment
                        .newInstance(toUri(getContext(),image_bmp).toString()));
            }
        }
    }
}