package com.oaj01.app.healthcareapp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.minh.app.healthcareapp.R;
import com.oaj01.app.healthcareapp.MainActivity;

import java.util.List;

public class TermsAndConditionsFragment extends Fragment{
    MainActivity main;
    RegisterViewModel viewModel;
    View view;
    Button backButton;
    Button acceptButton;
    ScrollView scrollView;
    TextView tsAndCs;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.terms_conditions_fragment, container, false);
        main = (MainActivity)getActivity();
        backButton = view.findViewById(R.id.backButton);
        acceptButton = view.findViewById(R.id.nextButton);
        scrollView = view.findViewById(R.id.scrollView);
        tsAndCs = view.findViewById(R.id.tsandcs);


        viewModel = ViewModelProviders.of(getActivity()).get(RegisterViewModel.class);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                main.getSupportFragmentManager().popBackStack();
            }
        });

        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewModel.acceptTerms();
                main.getSupportFragmentManager().popBackStack();
            }
        });

        String[] terms = getResources().getStringArray(R.array.terms_and_conditions);

        for(String term : terms){
            tsAndCs.append(term);
        }



    }
}
