package com.oaj01.app.healthcareapp.Modules.Parkinson;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import com.minh.app.healthcareapp.R;


public class ParkinsonsFragment extends Fragment{

    View view;
    TextView resultField;
    Button recordButton;
    Activity main;

    Recording recordingManager;



    //Recorder
    private Thread recordingThread = null;
    private boolean isRecording = false;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        if (Build.VERSION.SDK_INT < 26){
            view = inflater.inflate(R.layout.parkinsons_old_device, container, false);
        }
        else {

            view = inflater.inflate(R.layout.fragment_parkinsons, container, false);

            main = getActivity();
            resultField = view.findViewById(R.id.parkinsons_old_device);
            recordButton = view.findViewById(R.id.start_recording);

            recordingManager = new Recording(resultField, main);

            // Button listeners
            recordButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startRecord();
                }
            });


            resultField.setText("Press record and pronounce 'A' for 4 seconds");

            }
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }


    private void startRecord(){
        // Ask for recording permissions prior to recording
        if (ContextCompat.checkSelfPermission(getContext(),Manifest.permission.RECORD_AUDIO)
                == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.RECORD_AUDIO}, 1);
        } else {
            recordingManager.startRecording();
        }
    }
}
