package com.oaj01.app.healthcareapp.Modules.Parkinson;

import android.app.Activity;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.widget.TextView;

import com.oaj01.app.healthcareapp.MainActivity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class Recording {

        private static final int RECORDER_SAMPLERATE = 44100;
        private static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_STEREO;
        private static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;

        private AudioRecord recorder = null;
        private int bufferSize = 0;
        private boolean isRecording = false;
        private Thread recordingThread = null;

        private Activity main;

        TextView resultField;

        private WritingWav wavManager;
        private FeatureExtraction features;

    public Recording(TextView field, Activity activity){
            bufferSize = AudioRecord.getMinBufferSize(44100,
                    AudioFormat.CHANNEL_CONFIGURATION_MONO,
                    AudioFormat.ENCODING_PCM_16BIT);

            resultField = field;
            main = activity;
            wavManager = new WritingWav();
            features = new FeatureExtraction();
        }

        public void startRecording(){
            recorder = new AudioRecord(MediaRecorder.AudioSource.MIC,
                    RECORDER_SAMPLERATE, RECORDER_CHANNELS,RECORDER_AUDIO_ENCODING, bufferSize);

            int i = recorder.getState();
            if(i==1)
                recorder.startRecording();

            isRecording = true;
            resultField.setText("Recording...");

            recordingThread = new Thread(new Runnable() {

                @Override
                public void run() {
                    System.out.println("Recording started");
                    writeAudioDataToFile();
                }
            },"AudioRecorder Thread");

            Thread timerThread = new Thread(new Runnable() {

                @Override
                public void run() {
                    try {
                        TimeUnit.SECONDS.sleep(4);

                        main.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                stopRecording();
                                System.out.println("Recorded");
                            }
                        });

                    } catch (InterruptedException e) {
                        System.out.println("Thread is interuppted....");
                    }
                }
            }, "Timer Thread");


            recordingThread.start();
            timerThread.start();

        }

        private void stopRecording(){

            if(null != recorder){
                isRecording = false;
                resultField.setText("Predicting...");
                int i = recorder.getState();
                if(i==1)
                    recorder.stop();
                recorder.release();

                recorder = null;
                recordingThread = null;
            }
            wavManager.copyWaveFile(getTempFilename(),getFilename());
            deleteTempFile();


            double[] mfccs = features.retrieveMfccs(getFilename());
            for(double mfcc : mfccs){
                System.out.println(mfcc);
            }

            ModelManager model = new ModelManager();

            float prediction = model.predict(main, mfccs);
            if(prediction > 0.8){
                resultField.setText("Tremor was detected");
            }
            else{
                resultField.setText("No tremor detected");
            }

        }

        private void deleteTempFile() {
            File file = new File(getTempFilename());
            file.delete();
        }

        private void writeAudioDataToFile(){
            byte data[] = new byte[bufferSize];
            String filename = getTempFilename();
            FileOutputStream os = null;

            try {
                os = new FileOutputStream(filename);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            int read = 0;

            if(null != os){
                while(isRecording){
                    read = recorder.read(data, 0, bufferSize);

                    if(AudioRecord.ERROR_INVALID_OPERATION != read){
                        try {
                            os.write(data);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        private String getTempFilename(){
            String filepath = main.getCacheDir().getPath();

            File tempFile = new File(filepath , "temp_recording.wav");

            return (tempFile.getAbsolutePath());
        }

        private String getFilename(){
            String filepath = main.getCacheDir().getPath();

            File tempFile = new File(filepath , "recording.wav");

            return (tempFile.getAbsolutePath());
        }

}
