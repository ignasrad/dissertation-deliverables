package com.oaj01.app.healthcareapp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.minh.app.healthcareapp.R;
import com.oaj01.app.healthcareapp.MainActivity;

public class EnterCodeFragment extends Fragment {

    MainActivity main;
    ResetPasswordViewModel viewModel;
    View view;
    TextView codeField;
    Button back;
    Button upload;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        main = (MainActivity)getActivity();
        view = inflater.inflate(R.layout.enter_code_fragment, container, false);
        back = view.findViewById(R.id.back);
        upload = view.findViewById(R.id.uploadCode);
        codeField = view.findViewById(R.id.codeField);
        viewModel = ViewModelProviders.of(getActivity()).get(ResetPasswordViewModel.class);


        return view;
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //button listeners
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                main.getSupportFragmentManager().popBackStack();
            }
        });

        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = codeField.getText().toString();
                if(code.isEmpty()){
                    codeField.setError("Enter the code");
                    codeField.requestFocus();
                    return;
                }

                viewModel.setCode(code);
                main.pushNewFragment(new NewPasswordFragment());
            }
        });
    }
}
