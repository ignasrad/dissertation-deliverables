package com.oaj01.app.healthcareapp.Modules.common;

import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import androidx.exifinterface.media.ExifInterface;

public class Utils {
    // convert bitmap image to uri
    public static Uri toUri(Context context, Bitmap image) {
        Date date = new Date();
        String fileName = "blood_pressure_img_" + date + ".jpeg";
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), image, fileName, null);
        return Uri.parse(path);
    }

    // check if image is being rotated
    // if yes, return the degree needed to rotate back
    // return 0 otherwise
    public static Float getRotateDegree(ContentResolver contentResolver, Uri image_uri) {
        Float rotate_degree = 0F;
        ExifInterface exif_interface = null;
        try {
            InputStream input_stream = contentResolver.openInputStream(image_uri);
            exif_interface = new ExifInterface(input_stream);
            Log.d("debug","Here");
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert exif_interface != null;
        int img_orientation = exif_interface.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED);
        switch(img_orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                rotate_degree = 90F;
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                rotate_degree = 180F;
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                rotate_degree = 270F;
                break;
            default:
                break;
        }
        return rotate_degree;
    }
}