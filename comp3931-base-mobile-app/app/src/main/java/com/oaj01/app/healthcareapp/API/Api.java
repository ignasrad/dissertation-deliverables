package com.oaj01.app.healthcareapp.API;

import com.oaj01.app.healthcareapp.Model.DefaultResponse;
import com.oaj01.app.healthcareapp.Model.ExampleResponse;
import com.oaj01.app.healthcareapp.Model.LoginResponse;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;

public interface Api {
    @FormUrlEncoded
    @POST("users")
    Call<DefaultResponse> createUser(
            @Field("email") String email,
            @Field("password") String password,
            @Field("name") String name,
            @Field("date_of_birth") String dateOfBirth
    );

    @FormUrlEncoded
    @POST("authenticate")
    Call<LoginResponse> userLogin(
            @Field("email") String email,
            @Field("password") String password

    );


    @FormUrlEncoded
    @PUT("users/password-reset")
    Call<ResponseBody> passwordReset(
            @Field("email") String email
    );

    @FormUrlEncoded
    @PUT("users/password")
    Call<ResponseBody> changePassword(
            @Field("token") String token,
            @Field("new_password") String newPassword
    );

    @GET("example/labels")
    Call<ExampleResponse> example(
    );

    @FormUrlEncoded
    @POST("refresh")
    Call<ResponseBody> refreshToken();

    @FormUrlEncoded
    @POST("ocr_picture")
    Call<ResponseBody> sendPicture();
}
